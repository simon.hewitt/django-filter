from django.db import models
from django.db.models import F, ExpressionWrapper, Q, BooleanField
from django.db.models.functions import Coalesce



class GeoQueryManager(models.Manager):
    def get_queryset(self):
        new_qs = super().get_queryset()\
            .annotate(country_code=F('parent_geo__geo_name'))\
            .annotate(is_a_country=ExpressionWrapper(Q(geo_type=Geo.COUNTRY), output_field=BooleanField()))
        return new_qs


class Geo(models.Model):
    COUNTRY='CO'
    SITE='ST'
    GEO_TYPES = (
        (COUNTRY, 'Country'),
        (SITE, 'Site')
    )

    objects = GeoQueryManager()

    geo_name = models.CharField(max_length=32)
    old_country_code = models.CharField(max_length=2)
    geo_type = models.CharField(max_length=2,
                                choices=GEO_TYPES)

    parent_geo = models.ForeignKey('self',
                                   on_delete=models.CASCADE,
                                   null=True,
                                   blank=True,
                                   limit_choices_to={'is_a_country': True})

    def __repr__(self):
        return f"{self.geo_name} parent:{self.parent_geo}"