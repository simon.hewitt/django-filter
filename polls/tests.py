import pytest
from django.db.models import F

from polls.models import Geo


@pytest.mark.django_db
class TestGeo:

    @pytest.fixture
    def test_countries(self):
        c1 = Geo.objects.create(geo_name='UK',
                                old_country_code='UK',
                                geo_type=Geo.COUNTRY,
                                parent_geo=None)
        c2 = Geo.objects.create(geo_name='FR',
                                old_country_code='FR',
                                geo_type=Geo.COUNTRY,
                                parent_geo=None)
        s1 = Geo.objects.create(geo_name='Great Bedwyn',
                                old_country_code='UK',
                                geo_type=Geo.SITE,
                                parent_geo=c1)

        s2 = Geo.objects.create(geo_name='London',
                                old_country_code='UK',
                                geo_type=Geo.SITE,
                                parent_geo=c1)

        s3 = Geo.objects.create(geo_name='Paris',
                                old_country_code='FR',
                                geo_type=Geo.SITE,
                                parent_geo=c2)

        return {'France': c2,
                'UK': c1,
                's1': s1,
                's2': s2,
                's3': s3
                }

    def test_data(self, test_countries):
        assert Geo.objects.count() == 5
        assert Geo.objects.filter(geo_type=Geo.COUNTRY).count() == 2

    def test_create_geo_1(self):
        g1 = Geo.objects.create(geo_name='c1',
                                old_country_code='UK',
                                geo_type=Geo.COUNTRY)
        assert g1
        assert Geo.objects.count() == 1
        g1_db = Geo.objects.all().first()
        assert g1_db.geo_name == 'c1'

    def test_parents(self, test_countries):
        s1 = Geo.objects.get(geo_name='London')
        assert s1.parent_geo.geo_name == 'UK'
        assert s1.country_code == 'UK'
        s2 = Geo.objects.get(geo_name='UK')
        assert s2.parent_geo is None
        assert s2.country_code is None

    def test_filter_is_a_country(self, test_countries):
        is_cc = Geo.objects.filter(is_a_country=True)
        assert is_cc.count() == 2
        for c in is_cc.all():
            assert c.country_code is None
            assert c.geo_name in ['UK', 'FR']
            assert c.geo_type == Geo.COUNTRY
            assert c.is_a_country

        is_not_cc = Geo.objects.filter(is_a_country=False)
        assert is_not_cc.count() == 3
        for cc in is_not_cc.all():
            assert cc.country_code in ['UK', 'FR']
            assert not cc.is_a_country
            assert cc.geo_name in ['Great Bedwyn', 'Paris', 'London']
            assert isinstance(cc.parent_geo, Geo)
            c1 = cc.country_code
            c2 = cc.parent_geo.geo_name
            assert c1 == c2

    def test_filter_by_cc(self, test_countries):
        # Note this filter returns French non-country GeoEntities (just Paris)
        french = Geo.objects.filter(country_code='FR')
        assert french.count() == 1
        f1 = french.first()
        assert f1.geo_name == 'Paris'

        english = Geo.objects.filter(country_code='UK')
        assert english.count() == 2
        for eng in english:
            assert eng.geo_name in ['London', 'Great Bedwyn']
            assert not eng.is_a_country

        # All SITES in UK, FR (but not countries)
        lots = Geo.objects.filter(country_code__in=['FR', 'UK'])
        assert lots.count() == 3
        assert all(c.geo_type==Geo.SITE for c in lots.all())

    def test_annotate(self, test_countries):
        all_geo = Geo.objects.annotate(xx=(F('geo_type'))).all().first()
        assert all_geo.xx == 'CO'
        cnt = Geo.objects.annotate(xx=(F('geo_type'))).filter(xx='CO').count()
        assert cnt == 2

    def test_manager(self, test_countries):
        for cc in Geo.objects.all():
            ncc = cc.country_code
            oldcc = cc.old_country_code
            if cc.is_a_country:
                assert ncc is None
                assert cc.is_a_country
            else:
                assert ncc == oldcc
                assert not cc.is_a_country
