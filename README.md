# Django demo 

## to implement a calculated field that can be used to filer a query set.

simon hewitt

simon.d.hewitt@gmail.com

23/5/22

## Intro

I am keeping this demo as it illustrates some useful n tricky techniques

simon hewitt

May 2022



## V1 - manager

This Django project has no views or APIs. Its sole purpose is to implement and test a model.

The file in `polls/models.py`implements one model `class Geo`. This model represents sites and countries, and has a ForeignKey to itself to identify the parent country for sites. (The model does not stop countries referencing themselves).

In addition, `class GeoQueryManager(models.Manager):` adds 2 calculated fields `country_code` from `parent_geo__geo_name`, i.e. the name of the parent country, and `is_a_country:bool` implemented by `geo_type==Geo.COUNTRY` By adding in the manager in this way, these fields can be used in ORM filter statements (normal @property fields cannot be used in a filter)

File `polls/tests.py` tests these as filters and data fields, see the tests for examples of how they can be used.

As this is a demo before implementing in the GRID core, the main documentation is in the main repo: [maplkecroft](https://bitbucket.org/maplecroft/maplecroft/src/master/) at `docs/Jira-Tickets/API-211.md`. 



## V3 - simpler, using save()

In branch `v3-save`

